import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TicketsService} from "../../../services/tickets/tickets.service";
import {ITour} from "../../../models/tours";
import {TicketsStorageService} from "../../../services/tiсketstorage/tiсketstorage.service";
import {Router} from "@angular/router";
import {BlocksStyleDirective} from "../../../directive/blocks-style.directive";

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit {
tickets: ITour[];
loadCountBlock = false;

  @ViewChild('tourWrap', {read: BlocksStyleDirective}) blockDirective: BlocksStyleDirective;
  @ViewChild('tourWrap') tourWrap: ElementRef;

  constructor( private ticketService: TicketsService,
               private router: Router,
               private ticketStorage: TicketsStorageService) { }

  ngOnInit(): void {
    this.ticketService.getTickets().subscribe((data) => {
      this.tickets=data;
      this.ticketStorage.setStorage(data);
    }
    )
  }

  goToTicketInfoPage(item: ITour) {
    // this.router.navigate( ['/tickets/ticket/${item.id}'])
    // если пусть в роутинг модуле записан так: path: 'tickets/:id',

    this.router.navigate( ['/tickets/ticket'], {queryParams:{id:item.id}})
    // если пусть в роутинг модуле записан так: path: 'ticket',
  }

  directiveRenderComplete (ev:boolean) {
    const el: HTMLElement= this.tourWrap.nativeElement;
    el.setAttribute('style', 'background-color: #84b4ef')
    this.blockDirective.initStyle(3);
    this.loadCountBlock = true ;
  }
}


