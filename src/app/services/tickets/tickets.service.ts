import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {ITour} from "../../models/tours";
import {TicketsRestService} from "../rest/tickets-rest.service";

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  constructor(private ticketsServiceRest:  TicketsRestService) { }


 getTickets(): Observable<ITour[]>{
  return this.ticketsServiceRest.getTickets();
}

}
